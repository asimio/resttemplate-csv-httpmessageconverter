# README #

Accompanying source code for blog entry at https://tech.asimio.net/2022/01/13/Parsing-CSV-responses-with-a-custom-RestTemplate-HttpMessageConverter.html

### Requirements ###

* Java 8+
* Maven 3.2.x+

### Building the artifact ###

```
mvn clean package
```

### Running the application from command line ###

```
mvn spring-boot:run
```

### Available URLs

```
curl "http://localhost:8080/api/airplanes"
```
should result in successful JSON-formatted responses.

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero
