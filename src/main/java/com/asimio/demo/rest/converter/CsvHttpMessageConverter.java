package com.asimio.demo.rest.converter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractGenericHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import com.asimio.demo.csv.CsvParser;

public class CsvHttpMessageConverter<T> extends AbstractGenericHttpMessageConverter<List<T>> {

    private final CsvParser<T> csvParser;

    public CsvHttpMessageConverter(CsvParser<T> csvParser, MediaType... mediaTypes) {
        super(mediaTypes);
        this.csvParser = csvParser;
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return List.class.isAssignableFrom(clazz);
    }

    @Override
    protected void writeInternal(List<T> t, Type type, HttpOutputMessage outputMessage)
            throws IOException, HttpMessageNotWritableException {

        // NOOP
    }

    @Override
    public List<T> read(Type type, Class<?> contextClass, HttpInputMessage inputMessage)
            throws IOException, HttpMessageNotReadableException {

        return this.csvParser.parse(inputMessage.getBody());
    }

    @Override
    protected List<T> readInternal(Class<? extends List<T>> clazz, HttpInputMessage inputMessage)
            throws IOException, HttpMessageNotReadableException {

        return this.csvParser.parse(inputMessage.getBody());
    }

}