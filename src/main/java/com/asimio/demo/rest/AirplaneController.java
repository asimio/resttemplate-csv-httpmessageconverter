package com.asimio.demo.rest;

import java.util.Collections;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.asimio.demo.domain.Airplane;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(
        value = "/api/airplanes",
        produces = MediaType.APPLICATION_JSON_VALUE
)
@RequiredArgsConstructor
public class AirplaneController {

    private final RestTemplate restTemplate;

    @GetMapping(path = "")
    public ResponseEntity<List<Airplane>> findAirplanes() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.TEXT_PLAIN));

        HttpEntity<Void> request = new HttpEntity<>(headers);
        ResponseEntity<List<Airplane>> airplanes = this.restTemplate.exchange(
                "https://raw.githubusercontent.com/jpatokal/openflights/master/data/planes.dat",
                HttpMethod.GET,
                request,
                new ParameterizedTypeReference<List<Airplane>>(){}
        );
        // Do something else with airplanes
        return new ResponseEntity<>(airplanes.getBody(), HttpStatus.OK);
    }

}