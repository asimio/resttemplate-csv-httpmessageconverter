package com.asimio.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.asimio.demo.csv.CsvParser;
import com.asimio.demo.csv.impl.DefaultCsvParser;
import com.asimio.demo.domain.Airplane;
import com.asimio.demo.rest.converter.CsvHttpMessageConverter;

@Configuration
public class WebClientConfig {

    @Bean
    public CsvParser<Airplane> csvParser() {
        return new DefaultCsvParser<>(Airplane.class);
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate result = new RestTemplate();
        CsvHttpMessageConverter<Airplane> messageConverter = new CsvHttpMessageConverter<>(
                this.csvParser(),
                MediaType.ALL
        );
        result.getMessageConverters().add(messageConverter);
        return result;
    }

}